Title: Pelican on GitLab Pages + Firebase w/ Protected Environments
Date: 2022-11-17
Category: CI
Tags: pelican, gitlab, firebase
Slug: pages-firebase-deploy

This site is hosted on GitLab Pages and on Firebase. 

The source code of this site is at <https://gitlab.com/bcarranza/envs>.

Learn about GitLab Pages at <https://pages.gitlab.io>.
